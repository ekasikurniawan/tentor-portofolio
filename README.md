---

Tentor Website HTML Project (Portofolio).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Description](#desc)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Description

This project was created while I was working at a company called Tentor.

To maintain the credibility of the company, this project will only be 'read-only'.

The above example expects to put all your HTML files in the `public/` directory.

For Production site, check [Tentor](https://tentor.co.id) Site.